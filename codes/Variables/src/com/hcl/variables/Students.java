package com.hcl.variables;

public class Students {
	private int setid;
	private String setname;
	private double percentage;
	public Students(int id,String name,double percentage) {
		super();
		this.setid=id;
		this.setname=name;
		this.percentage=percentage;
	}
	public int getSetid() {
		return setid;
	}
	public void setSetid(int setid) {
		this.setid = setid;
	}
	public String getSetname() {
		return setname;
	}
	public void setSetname(String setname) {
		this.setname = setname;
	}
	public double getPercentage() {
		return percentage;
	}
	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}
	

}
