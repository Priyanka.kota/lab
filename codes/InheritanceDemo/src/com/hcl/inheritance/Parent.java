package com.hcl.inheritance;

public class Parent {
	int pid = 100;

	public Parent() {
		super();
	}

	public String methodOne() {
		System.out.println("m1() from parent");
		return null;
	}

	public String toString() {
		System.out.println("Iam a parent");
		return null;
	}

}
