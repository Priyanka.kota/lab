package com.hcl.inheritance;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Parent p = new Parent();
		System.out.println(p.pid);
		p.methodOne();
		System.out.println(p.toString());
		String s = new String("kota");
		System.out.println(s.toString());
		Integer i = new Integer(56);
		System.out.println(i.toString());
		Child c = new Child();
		System.out.println(c.cid);
		System.out.println(c.pid);
		c.methodOne();
		Object obj = new Object();
		obj = new Parent();
		obj = new Child();
		Parent p1 = new Parent();
		p1 = new Child();
		p1.methodOne();
		Child c1 = new Child();
	}

}
