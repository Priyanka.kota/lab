package com.hcl.oops;
import java.lang.*;
public class OopsDemo {
public OopsDemo() {
	System.out.println("default constructor");
}
private int rno;
private String name;
public OopsDemo(int id,String name) {
	System.out.println("parameterized constructor");
	System.out.println("Id name is:"+""+ id);
	System.out.println("My Name is:"+""+ name);
	OopsDemo.hello();
}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		OopsDemo demo=new OopsDemo();
		System.out.println(demo);
		OopsDemo demo1=new OopsDemo(1,"priya");
		/*System.out.println(demo1);*/
		
	}
	public static  void hello() {
		System.out.println("hello");
	}

}
