package com.hcl;

public class Strings {

	public static void main(String[] args) {
		String str1 = "Java";
		String str2 = new String("priya");
		str2 = "hema";
		str2 = "laxmi";
		System.out.println(str2);
		System.out.println(str1.length());
		System.out.println(str1.charAt(0));
		System.out.println(str2.toUpperCase());
		String  s1 = "A";
		String s3 = "A";
		System.out.println(s1.equals(s3)); 
		System.out.println(s1 ==  s3);
		String s4 = "A";
		String s2 = new String("B");
		String s5 = new String("B");		
		System.out.println(s2.equals(s5));
		System.out.println(s2 == s5);
		int x = 5 , y =5;
		System.out.println(x == y);
	}

}
