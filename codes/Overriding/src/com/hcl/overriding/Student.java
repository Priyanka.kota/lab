package com.hcl.overriding;

public class Student {
	private String name;
	private int rollnum;
	private int marks;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRollnum() {
		return rollnum;
	}

	public void setRollnum(int rollnum) {
		this.rollnum = rollnum;
	}

	public int getMarks() {
		return marks;
	}

	public void setMarks(int marks) {
		this.marks = marks;
	}

	public String toString() {
		return this.name + "\n" + this.rollnum;
	}

	public boolean equals(Object obj) {
		boolean flag = false;
		Student names = (Student) obj;
		/*
		 * if(this.rollnum=names.rollnum) { flag=true; }
		 */
		return flag;

	}
}
